class Base {
    render({ title, pageStyle, content }) {
        if (title) {
            title += ' | Jonathan Troyer'
        } else {
            title = 'Jonathan Troyer'
        }
        /*html*/
        return `
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="Content-Security-Policy" content="default-src 'self'; script-src 'none'; style-src 'self' 'unsafe-hashes' 'sha256-/3kWSXHts8LrwfemLzY9W0tOv5I4eLIhrf0pT8cU0WI=' 'sha256-mf/UeN4J7RwvsimPJmmeFQFxedoyNr/nO9Q1L1vCL7k=' 'sha256-ogGZDOBslrjBuR0sUGSVyE8am41DJy05qzt8tyzcnNs='">
                <meta name="theme-color" content="#c0ffc0">
                
                <title>${title}</title>
                <link rel="shortcut icon" href="favicon.ico" />

                <link rel="preload" as="font" crossorigin type="font/ttf" href="/assets/fonts/PublicSans-VariableFont_wght.ttf">
                <link rel="preload" as="style" href="/styles/base.css">
                <link rel="preload" as="style" href="/styles/layout.css">

                <link rel="stylesheet" href="/styles/base.css">
                <link rel="stylesheet" href="/styles/layout.css">
                ${pageStyle ? `<style>${pageStyle}</style>` : ''}
            </head>
            <body>
                <header>
                    <nav></nav>
                    //TODO: nav (not really useful until I have more than a homepage though)
                </header>
                ${content}
                <footer class="flex center">
                    <div class="flex icons">
                        <a href="https://gitlab.com/JonathanTroyer">
                            <svg aria-hidden="true"><use href="assets/icons/gitlab.svg"></svg>
                            <span class="sr-only">GitLab</span>
                        </a>
                        <a href="https://github.com/JonathanTroyer">
                            <svg aria-hidden="true"><use href="assets/icons/github.svg"></svg>
                            <span class="sr-only">GitHub</span>
                        </a>
                        <a href="https://twitter.com/MrJTroyer">
                            <svg aria-hidden="true"><use href="assets/icons/twitter.svg"></svg>
                            <span class="sr-only">Twitter</span>
                        </a>
                    </div>
                </footer>
            </body>
            </html>
        `
    }
}


module.exports = Base;
