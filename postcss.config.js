const normalize = require('postcss-normalize')
const postcssPresetEnv = require('postcss-preset-env')({
    stage: 0
})

const purgeHtml = require('purgecss-from-html');
const purgecss = require('@fullhuman/postcss-purgecss')({
    content: [
        './_site/**/*.html',
    ],
    css: [], // css
    extractors: [
        {
            extractor: purgeHtml,
            extensions: ['html']
        }
    ]
})

module.exports = {
    // modules: true,
    plugins: [
        normalize,
        postcssPresetEnv,
        ...(process.env.NODE_ENV === 'production'
            ? [purgecss]
            : [])
    ]
}
